const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", (req, res) => {

	// .then method uses the result from the controller function and sends it back to the frontend application via res.send method
	userController.checkEmailExists(req.body).then(resultFromController => res.send (resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController));
});

// Route for the user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController));
}); 

// Route for retrieving user details
// The "auth.verify" acts as a middleware to ensure that the user is logged in first before they retrieve the details
router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the auth.js to retrieve the user information from the token, passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// // Route to enroll a user to a course
// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));
// });


// Route to enroll a user to a course - ACTIVITY
router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId : userData.id,
		courseId : req.body.courseId
	}

	if (userData.isAdmin == false){		

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(false)
	}
});




// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;