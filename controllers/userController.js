const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");

// Check if the email already exists
/*
	Steps:
	1. Use "mongoose" "find method" to find duplicate emails
	2. .then method to send a response back to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		// The find method return a record if a match is found
		if (result.length > 0) {
			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		}
	})
};


// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provid all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,

		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// saves the created object to our database
	return newUser.save().then((user, error) => {

		// User registration failed
		if (error) {
			return false;

		// User registration is successful
		} else {
			return true;
		}
	})
};

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {

			// Creates a variable to return the result of comparing the login form password and the database password
			// "compareSync" is used to compare a non-encrypted password from the login form to the encrypted password from the database and return "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the password matches
			if(isPasswordCorrect) {

				// Generates an access token
				// it uses the "createAccessToken" method that we defined in the auth.js file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// Retrieve user details (Activity)
/*
	Steps :
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (reqBody) => {

	return User.findById(reqBody.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
	
};

// Enroll a user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId})

		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	});

	if (isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	}
};


